<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $characteristic_id
 * @property int $user_animal_id
 * @property boolean $value
 * @property string $last_fall
 * @property string $last_up
 * @property string $created_at
 * @property string $updated_at
 * @property Characteristic $characteristic
 * @property UserAnimal $userAnimal
 */
class AnimalCharacteristic extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'animal_characteristic';

    /**
     * @var array
     */
    protected $fillable = ['characteristic_id', 'user_animal_id', 'value', 'last_fall', 'last_up', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function characteristic()
    {
        return $this->belongsTo('App\Characteristic');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userAnimal()
    {
        return $this->belongsTo('App\UserAnimal');
    }
}
