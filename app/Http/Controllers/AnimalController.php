<?php

namespace App\Http\Controllers;

use App\User;
use App\UserAnimal;
use Auth;
use DB;

class AnimalController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function indexAction()
    {
        return UserAnimal::with(['animal', 'animalCharacteristics.characteristic'])->get();
    }

    public function useAction($characteristicId, $userAnimalId)
    {
        /** @var UserAnimal $userAnimal */
        $userAnimal = UserAnimal::findOrFail($userAnimalId);
        //check users disabled
//        $user = Auth::user();
//        if(!$user->can('update', $userAnimal)) {
//            abort(403);
//        }
        $userAnimal->up($characteristicId);

        User::sendUpdates();
    }

    public function createAction($animalId)
    {
        DB::beginTransaction();
        $animal = UserAnimal::create(['animal_id' => $animalId, 'user_id' => 1]);
        DB::commit();

        User::sendUpdates();
    }

    public function sendPush() {
        User::sendUpdates();
    }
}
