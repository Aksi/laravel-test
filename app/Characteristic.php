<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $title
 * @property int $fall_time
 * @property integer $min
 * @property integer $max
 * @property integer $up
 * @property integer $timeout
 * @property boolean $die
 * @property AnimalCharacteristic[] $animalCharacteristics
 * @property CharacteristicCondition[] $characteristicConditions
 */
class Characteristic extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'characteristic';

    /**
     * @var array
     */
    protected $fillable = ['title', 'fall_time', 'min', 'max', 'die'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function animalCharacteristics()
    {
        return $this->hasMany('App\AnimalCharacteristic');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function characteristicConditions()
    {
        return $this->hasMany('App\CharacteristicCondition', 'target_id');
    }
}
