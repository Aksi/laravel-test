<?php

namespace App\Console\Commands;

use App\AnimalCharacteristic;
use App\Characteristic;
use App\CharacteristicCondition;
use App\Events\AnimalEvent;
use App\User;
use App\UserAnimal;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;

class AnimalsCharacteristic extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'animal:characteristic';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reduce Animal Chars';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /** @var AnimalCharacteristic[] $animalCharacteristics */
        $animalCharacteristics = AnimalCharacteristic::with('characteristic')->lockForUpdate()->get();
        if(!count($animalCharacteristics)) {
            return;
        }
        $charsCount = Characteristic::count();
        $animalCharacteristics = collect($animalCharacteristics);
        $animalCharacteristics = $animalCharacteristics->sortBy('animal_id');
        $animalCharacteristics = $animalCharacteristics->chunk($charsCount);

        $conditions = CharacteristicCondition::all();
        $conditions = collect($conditions);

        foreach ($animalCharacteristics as $animalCharacteristic) {
            $animalCondition = [];
            /** @var AnimalCharacteristic $animalChar */
            foreach ($animalCharacteristic as $animalChar) {
                /** @var CharacteristicCondition $condition */
                foreach($conditions as $condition) {
                    if($condition->characteristic_id == $animalChar->characteristic->id
                        && $condition->to >= $animalChar->value)
                    {
                        $animalCondition[$condition->target_id] = [
                            'timeout' => $condition->fall_time,
                            'reduce' => $condition->value
                        ];
                    }
                }
            }
            /** @var AnimalCharacteristic $animalChar */
            foreach ($animalCharacteristic as $animalChar) {
                /** @var Characteristic $characteristic */
                $characteristic = $animalChar->characteristic;

                /** Default */
                $timeout = $characteristic->timeout;
                $reduce = 1;
                /** Condition */
                if(isset($animalCondition[$characteristic->id])) {
                    $timeout = $animalCondition[$characteristic->id]['timeout'];
                    $reduce = $animalCondition[$characteristic->id]['reduce'];
                }
                if(strtotime($animalChar->last_fall) < time() - 60 * $timeout) {
                    $animalChar->value -= $reduce;
                    $animalChar->last_fall = new \DateTime();
                    $animalChar->save();
                }
            }
        }
        User::sendUpdates();

    }
}
