<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $title
 * @property UserAnimal[] $userAnimals
 */
class Animal extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'animal';

    /**
     * @var array
     */
    protected $fillable = ['title'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userAnimals()
    {
        return $this->hasMany('App\UserAnimal');
    }
}
