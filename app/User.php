<?php

namespace App;

use App\Events\AnimalEvent;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'login'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public static function sendUpdates()
    {
        $event = new AnimalEvent(UserAnimal::with(['animal', 'animalCharacteristics.characteristic'])->get());
        event($event);
    }
}
