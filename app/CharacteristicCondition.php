<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $characteristic_id
 * @property int $target_id
 * @property integer $to
 * @property integer $value
 * @property int $fall_time
 * @property Characteristic $characteristic
 * @property Characteristic $targetCharacteristic
 */
class CharacteristicCondition extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'characteristic_condition';

    /**
     * @var array
     */
    protected $fillable = ['characteristic_id', 'target_id', 'to', 'value', 'fall_time'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function characteristic()
    {
        return $this->belongsTo('App\Characteristic');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function targetCharacteristic()
    {
        return $this->belongsTo('App\Characteristic', 'target_id');
    }
}
