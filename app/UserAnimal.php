<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $animal_id
 * @property string $created_at
 * @property string $updated_at
 * @property Animal $animal
 * @property User $user
 * @property AnimalCharacteristic[] $animalCharacteristics
 */
class UserAnimal extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'user_animal';

    protected static function boot()
    {
        parent::boot();

        /** create all characteristics */
        static::saved(function($userAnimal){
            $chars = Characteristic::all();
            foreach($chars as $char) {
                AnimalCharacteristic::create([
                    'user_animal_id' => $userAnimal->id,
                    'characteristic_id' => $char->id,
                    'value' => $char->max
                ]);
            }
        });
    }

    /**
     * @var array
     */
    protected $fillable = ['animal_id', 'user_id', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function animal()
    {
        return $this->belongsTo('App\Animal');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function animalCharacteristics()
    {
        return $this->hasMany('App\AnimalCharacteristic');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Up Animal Char
     *
     * @param integer $characteristicId
     */
    public function up($characteristicId)
    {
        /** @var AnimalCharacteristic $animalChar */
        $animalChar = $this->animalCharacteristics()->where('characteristic_id', $characteristicId)->lockForUpdate()->firstOrFail();
        $characteristic = $animalChar->characteristic;
        $animalChar->value += $characteristic->up;
        $animalChar->save();
    }
}
