<?php

namespace App\Policies;

use App\User;
use App\UserAnimal;
use Illuminate\Auth\Access\HandlesAuthorization;

class AnimalPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the userAnimal.
     *
     * @param  \App\User  $user
     * @param  \App\UserAnimal  $userAnimal
     * @return mixed
     */
    public function view(User $user, UserAnimal $userAnimal)
    {
        //
    }

    /**
     * Determine whether the user can create userAnimals.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the userAnimal.
     *
     * @param  \App\User  $user
     * @param  \App\UserAnimal  $userAnimal
     * @return mixed
     */
    public function update(User $user, UserAnimal $userAnimal)
    {
        return $user->id == $userAnimal->user_id;
    }

    /**
     * Determine whether the user can delete the userAnimal.
     *
     * @param  \App\User  $user
     * @param  \App\UserAnimal  $userAnimal
     * @return mixed
     */
    public function delete(User $user, UserAnimal $userAnimal)
    {
        //
    }
}
