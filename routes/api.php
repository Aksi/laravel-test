<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return \App\User::find(1);
});

Route::get('push', 'AnimalController@sendPush');

Route::get('/animals', 'AnimalController@indexAction');

Route::
    get('/animal/{userAnimalId}/{characteristicId}', 'AnimalController@useAction')
    ->where('userAnimalId', '[0-9]+')
    ->where('characteristicId', '[0-9]+');

Route::get('/animal/{animalId}', 'AnimalController@createAction')
    ->where('animalId', '[0-9]+');