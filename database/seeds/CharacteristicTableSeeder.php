<?php

use Illuminate\Database\Seeder;

class CharacteristicTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('characteristic')->insert([
            [
                'title' => 'Голод',
                'fall_time' => 10,
                'min' => -2,
                'max' => 100,
                'timeout' => 5,
                'die' => true
            ],
            [
                'title' => 'Сон',
                'fall_time' => 20,
                'min' => 0,
                'max' => 100,
                'timeout' => 10,
                'die' => false
            ],
            [
                'title' => 'Забота',
                'fall_time' => 15,
                'min' => 0,
                'max' => 100,
                'timeout' => 5,
                'die' => false
            ],
            [
                'title' => 'Веселье',
                'fall_time' => 0,
                'min' => 0,
                'max' => 100,
                'timeout' => 20,
                'die' => false
            ]
        ]);
    }
}
