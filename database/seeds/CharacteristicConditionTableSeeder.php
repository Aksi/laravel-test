<?php

use Illuminate\Database\Seeder;

class CharacteristicConditionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('characteristic_condition')->insert([
            [
                'characteristic_id' => 2,
                'to' => 4,
                'target_id' => 3,
                'value' => 3,
                'fall_time' => 5
            ],
        ]);
    }
}
