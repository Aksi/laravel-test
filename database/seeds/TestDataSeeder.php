<?php

use Illuminate\Database\Seeder;

class TestDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user')->insert(['login' => 'test']);
        DB::table('user_animal')->insert(['user_id' => 1, 'animal_id' => 3]);
        DB::table('animal_characteristic')->insert([
            [
                'user_animal_id' => 1,
                'characteristic_id' => 1,
                'value' => 48,
                'last_fall' => '2017-08-04 17:59:01',
                'created_at' => '2017-08-04 17:59:01',
                'updated_at' => '2017-08-04 17:59:01',
            ],
            [
                'user_animal_id' => 1,
                'characteristic_id' => 2,
                'value' => 78,
                'last_fall' => '2017-08-04 17:59:01',
                'created_at' => '2017-08-04 17:59:01',
                'updated_at' => '2017-08-04 17:59:01',
            ],
            [
                'user_animal_id' => 1,
                'characteristic_id' => 3,
                'value' => 24,
                'last_fall' => '2017-08-04 17:59:01',
                'created_at' => '2017-08-04 17:59:01',
                'updated_at' => '2017-08-04 17:59:01',
            ],
            [
                'user_animal_id' => 1,
                'characteristic_id' => 4,
                'value' => 11,
                'last_fall' => '2017-08-04 17:59:01',
                'created_at' => '2017-08-04 17:59:01',
                'updated_at' => '2017-08-04 17:59:01',
            ]
        ]);
    }
}
