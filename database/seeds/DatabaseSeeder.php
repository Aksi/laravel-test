<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AnimalTableSeeder::class);
        $this->call(CharacteristicTableSeeder::class);
        $this->call(CharacteristicConditionTableSeeder::class);
        $this->call(TestDataSeeder::class);
    }
}
