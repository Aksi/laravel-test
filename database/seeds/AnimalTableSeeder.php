<?php

use Illuminate\Database\Seeder;

class AnimalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('animal')->insert([
            ['title' => 'Собака'],
            ['title' => 'Кот'],
            ['title' => 'Енот'],
            ['title' => 'Пингвин']
        ]);
    }
}
