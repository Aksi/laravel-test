<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCharacteristicTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('characteristic', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 50);
            $table->unsignedInteger('fall_time')->comment('Fall time in minutes. 0 - disabled');
            $table->smallInteger('min');
            $table->smallInteger('max');
            $table->smallInteger('up')->default(1);
            $table->smallInteger('timeout')->comment('In minutes');
            $table->boolean('die');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('characteristic');
    }
}
