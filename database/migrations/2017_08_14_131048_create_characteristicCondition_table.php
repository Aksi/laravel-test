<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCharacteristicConditionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('characteristic_condition', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('characteristic_id', false, true);
            $table->smallInteger('to')->comment('Rule works when value greater than or equals');
            $table->integer('target_id', false, true);
            $table->smallInteger('value');
            $table->unsignedInteger('fall_time')->comment('Fall time in minutes. 0 - disabled');

            $table->foreign('characteristic_id')->references('id')->on('characteristic');
            $table->foreign('target_id')->references('id')->on('characteristic');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('characteristic_condition');
    }
}
