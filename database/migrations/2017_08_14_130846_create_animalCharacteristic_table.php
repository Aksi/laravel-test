<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnimalCharacteristicTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('animal_characteristic', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_animal_id', false, true);
            $table->integer('characteristic_id', false, true);
            $table->tinyInteger('value');
            $table->timestamp('last_fall')->useCurrent();
            $table->timestamp('last_up')->useCurrent();
            $table->timestamps();

            $table->foreign('user_animal_id')->references('id')->on('user_animal');
            $table->foreign('characteristic_id')->references('id')->on('characteristic');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animal_characteristic');
    }
}
